export class PipelineLinkTo {
  public id: number;
  public name: string;
  public sourceNodeUuid: string;
  public targetNodeUuid: string;
}
