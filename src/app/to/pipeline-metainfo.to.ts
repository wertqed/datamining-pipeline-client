export class PipelineMetainfoTo {
  public uuid: string;
  public name: string;
  public dataTypeCode: number;
  public value: any;
  public fileName: any;
  public description: string;
}
