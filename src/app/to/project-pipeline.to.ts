import {PipelineStepTo} from "./pipeline-step.to";
import {PipelineLinkTo} from "./pipeline-link.to";

export class ProjectPipelineTo {
  public projectId: number;
  public pipelineSteps: PipelineStepTo[];
  public pipelineLinks: PipelineLinkTo[];
}
