import {PipelineMetainfoTo} from "./pipeline-metainfo.to";

export class PipelineStepTo {
  public id: number;
  public uuid: string;
  public name: string;
  public serviceId: number;
  public metaInfo: PipelineMetainfoTo[];
}
