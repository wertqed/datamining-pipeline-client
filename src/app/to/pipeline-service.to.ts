import {PipelineMetainfoTo} from "./pipeline-metainfo.to";

export class PipelineServiceTo {
  id: number;
  name: string;
  groupCode: string;
  host: string;
  port: string;
  path: string;
  inDataTypeCode: string;
  outDataTypeCode: string;
  metaInfo: PipelineMetainfoTo[];
}
