import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';

import {AppRoutingModule} from './modules/app-routing.module';
import {ServiceLogComponent} from './components/service-log/service-log.component';
import {RouterModule} from "@angular/router";
import {HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {
  MatExpansionModule,
  MatIconModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule, MatTabsModule
} from '@angular/material';
import {AppComponent} from "./components/app/app.component";
import {NavigationPanelComponent} from './components/navigation-panel/navigation-panel.component';
import {MaterialModule} from "./modules/material.module";
import {ApiRequestService} from "./services/api-request.service";
import {PiplineservServiceService} from "./services/piplineserv-service.service";
import {PiplineModalWindowComponent} from './components/service-log/pipline-modal-window/pipline-modal-window.component';
import {ProjectLogComponent} from './components/project-log/project-log.component';
import {ProjectModalWindowComponent} from './components/project-modal-window/project-modal-window.component';
import {ProjectPiplineComponent} from './components/project-pipline/project-pipline.component';
import {DragDropModule} from "@angular/cdk/drag-drop";
import {PipelineResultWindowComponent} from './components/project-pipeline-graph/pipeline-result-window/pipeline-result-window.component';
import {ProjectPipelineGraphComponent} from './components/project-pipeline-graph/project-pipeline-graph.component';
import {NgxGraphModule} from '@swimlane/ngx-graph';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import { PipeManageWindowComponent } from './components/project-pipeline-graph/pipe-manage/pipe-manage-window.component';
import {DictionaryService} from "./services/dictionary.service";
import { MetaInfoInputComponent } from './components/project-pipeline-graph/meta-info-input/meta-info-input.component';

@NgModule({
  declarations: [
    AppComponent,
    ServiceLogComponent,
    NavigationPanelComponent,
    PiplineModalWindowComponent,
    ProjectLogComponent,
    ProjectModalWindowComponent,
    ProjectPiplineComponent,
    PipelineResultWindowComponent,
    ProjectPipelineGraphComponent,
    PipeManageWindowComponent,
    MetaInfoInputComponent,
    PipeManageWindowComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatExpansionModule,
    MatIconModule,
    MatTabsModule,
    DragDropModule,
    NgxGraphModule,
    NgxChartsModule
  ],
  providers: [ApiRequestService,
    PiplineservServiceService,
  DictionaryService],
  bootstrap: [AppComponent],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  entryComponents: [
    PiplineModalWindowComponent,
    ProjectModalWindowComponent,
    PipelineResultWindowComponent
  ],
})
export class AppModule {
}
