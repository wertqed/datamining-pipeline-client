import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ServiceLogComponent} from "../components/service-log/service-log.component";
import {ProjectLogComponent} from "../components/project-log/project-log.component";
import {ProjectPiplineComponent} from "../components/project-pipline/project-pipline.component";
import {ProjectPipelineGraphComponent} from "../components/project-pipeline-graph/project-pipeline-graph.component";

const routes: Routes = [
  {path: '', redirectTo: '/projects-log', pathMatch: 'full'},
  {
    path: 'services-log',
    component: ServiceLogComponent
  }, {
    path: 'projects-log',
    component: ProjectLogComponent
  }, {
    path: 'project-pipeline/:id',
    component: ProjectPiplineComponent
  }, {
    path: 'project-pipeline-network/:id',
    component: ProjectPipelineGraphComponent
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})]
  // exports: [RouterModule]
})
export class AppRoutingModule {
}
