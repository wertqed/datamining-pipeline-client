import { Injectable } from '@angular/core';
import {ApiRequestService} from "./api-request.service";
import {Observable} from "rxjs";
import {PipelineServiceTo} from "../to/pipeline-service.to";
import {DictionaryTo} from "../to/dictionary.to";

@Injectable({
  providedIn: 'root'
})
export class DictionaryService {

  private basePath = "dict";

  constructor(private apiRequest: ApiRequestService) {
  }

  getDataTypes():Observable<DictionaryTo[]> {
    return this.apiRequest.get(this.basePath + '/datatype/list');
  }

  getServiceGroupTypes():Observable<DictionaryTo[]> {
    return this.apiRequest.get(this.basePath + '/servicegroups/list');
  }
}
