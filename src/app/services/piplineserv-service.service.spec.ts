import { TestBed } from '@angular/core/testing';

import { PiplineservServiceService } from './piplineserv-service.service';

describe('PiplineservServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PiplineservServiceService = TestBed.get(PiplineservServiceService);
    expect(service).toBeTruthy();
  });
});
