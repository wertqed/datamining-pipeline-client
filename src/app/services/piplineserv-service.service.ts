import {Injectable} from '@angular/core';
import {ApiRequestService} from "./api-request.service";
import {PipelineServiceTo} from "../to/pipeline-service.to";
import {Observable} from "rxjs";

@Injectable()
export class PiplineservServiceService {

  private basePath = "pipelineservices";

  constructor(private apiRequest: ApiRequestService) {
  }

  /**
   * Returns list of evaluation reports
   * @returns {any}
   */
  getAllServices():Observable<PipelineServiceTo[]> {
    return this.apiRequest.get(this.basePath + '/list');
  }

  save(service: PipelineServiceTo) {
    return this.apiRequest.post(this.basePath + '/save', service);
  }

  delete(id) {
    return this.apiRequest.delete(this.basePath + '/' + id);
  }
}
