import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

@Injectable()
export class ApiRequestService {

  private baseApiPath = "http://localhost:8080/";

  constructor(private http: HttpClient) {
  }

  getHeaders(): HttpHeaders {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    return headers;
  }

  get(url: string, urlParams?: HttpParams): Observable<any> {
    let me = this;
    //noinspection TypeScriptValidateTypes
    return this.http.get(this.baseApiPath + url, {headers: this.getHeaders()})
      .pipe(map(data => {
          return data;
        }),
        catchError(function (error: any) {
          console.log('Http PUT request error: ' + error ? error.message : '');
          return throwError(error || 'Ошибка выполнения http get запроса');
        })
      );
  }

  post(url: string, body: Object): Observable<any> {
    let me = this;
    //noinspection TypeScriptUnresolvedFunctio
    return this.http.post(this.baseApiPath + url, JSON.stringify(body), {headers: this.getHeaders()})
      .pipe(map(data => {
          return data;
        }),
        catchError(function (error: any) {
          console.log('Http PUT request error: ' + error ? error.message : '');
          return throwError(error || 'Ошибка выполнения http post запроса');
        })
      );
  }

  put(url: string, body: Object, urlParams?: HttpParams): Observable<any> {
    let me = this;
    //noinspection TypeScriptUnresolvedFunction
    return this.http.put(this.baseApiPath + url, JSON.stringify(body), {headers: this.getHeaders(), params: urlParams})
      .pipe(map(data => {
          return data;
        }),
        catchError(function (error: any) {
          console.log('Http PUT request error: ' + error ? error.message : '');
          return throwError(error || 'Ошибка выполнения http PUT запроса');
        })
      );
  }

  delete(url: string): Observable<any> {
    let me = this;
    //noinspection TypeScriptValidateTypes
    return this.http.delete(this.baseApiPath + url, {headers: this.getHeaders()})
      .pipe(map(data => {
          return data;
        }),
        catchError(function (error: any) {
          console.log('Http PUT request error: ' + error ? error.message : '');
          return throwError(error || 'Ошибка выполнения http delete запроса');
        })
      );;
  }

}
