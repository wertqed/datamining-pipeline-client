import {Injectable} from '@angular/core';
import {ApiRequestService} from "./api-request.service";
import {ProjectTo} from "../to/project.to";
import {ProjectPipelineTo} from "../to/project-pipeline.to";

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  private basePath = "project";

  constructor(private apiRequest: ApiRequestService) {
  }

  /**
   * Returns list of evaluation reports
   * @returns {any}
   */
  getAllProjects() {
    return this.apiRequest.get(this.basePath + '/list');
  }

  save(project: ProjectTo) {
    return this.apiRequest.post(this.basePath + '/save', project);
  }

  delete(id) {
    return this.apiRequest.delete(this.basePath + '/' + id);
  }

  savePipeline(pipeline: ProjectPipelineTo) {
    return this.apiRequest.post(this.basePath + '/save/pipeline', pipeline);
  }

  getPipeline(projectId) {
    return this.apiRequest.get(this.basePath + '/' + projectId + '/pipeline');
  }

  startPipeline(projectId, stepUuid) {
    return this.apiRequest.get(this.basePath + '/start/' + projectId + '/' + stepUuid);
  }

  getResult(stepUuid) {
    return this.apiRequest.get(this.basePath + '/result/' + stepUuid);
  }
}

