import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PiplineModalWindowComponent } from './pipline-modal-window.component';

describe('PiplineModalWindowComponent', () => {
  let component: PiplineModalWindowComponent;
  let fixture: ComponentFixture<PiplineModalWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PiplineModalWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PiplineModalWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
