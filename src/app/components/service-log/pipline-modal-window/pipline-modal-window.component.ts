import {Component, EventEmitter, Inject, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {PipelineServiceTo} from "../../../to/pipeline-service.to";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {PiplineservServiceService} from "../../../services/piplineserv-service.service";
import {PipelineMetainfoTo} from "../../../to/pipeline-metainfo.to";
import {Observable} from "rxjs";
import {DictionaryService} from "../../../services/dictionary.service";
import {UuidUtils} from "../../../utils/uuid-utils";

@Component({
  selector: 'app-pipline-modal-window',
  templateUrl: './pipline-modal-window.component.html',
  styleUrls: ['./pipline-modal-window.component.sass'],
  encapsulation: ViewEncapsulation.None
})
export class PiplineModalWindowComponent implements OnInit {

  record: PipelineServiceTo;
  dataTypes = [];
  groupTypes = [];
  @Output()
  public refreshEvent: EventEmitter<null> = new EventEmitter();

  private pipelineMetaInfo: PipelineMetainfoTo[] = [];

  constructor(public dialogRef: MatDialogRef<PiplineModalWindowComponent>,
              @Inject(MAT_DIALOG_DATA) public inRecord: PipelineServiceTo,
              private pipelineService: PiplineservServiceService,
              private dictionaryService: DictionaryService) {
    this.record = inRecord;
    this.pipelineMetaInfo = inRecord.metaInfo ? inRecord.metaInfo : [];
  }

  ngOnInit() {
    this.dictionaryService.getDataTypes().subscribe(dict => {
      this.dataTypes = dict;
    });
    this.dictionaryService.getServiceGroupTypes().subscribe(dict => {
      this.groupTypes = dict;
    });
  }

  save() {
    this.record.metaInfo = this.pipelineMetaInfo;
    this.pipelineService.save(this.record).subscribe(resp => {
      this.refreshEvent.emit();
    });
  }

  addMetaElement() {
    let metainfoTo = new PipelineMetainfoTo();
    metainfoTo.uuid = UuidUtils.genUuid();
    this.pipelineMetaInfo.push(metainfoTo);
  }

  delete(metaElement: any) {
    let index = this.pipelineMetaInfo.findIndex((elem: PipelineMetainfoTo) => {
      return elem.uuid == metaElement.uuid;
    });
    this.pipelineMetaInfo.splice(index, 1);
  }
}
