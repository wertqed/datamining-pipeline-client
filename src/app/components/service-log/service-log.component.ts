import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {PiplineservServiceService} from "../../services/piplineserv-service.service";
import {BehaviorSubject, merge, Observable} from "rxjs";
import {DataSource} from "@angular/cdk/table";
import {CollectionViewer} from "@angular/cdk/collections";
import {PipelineServiceTo} from "../../to/pipeline-service.to";
import {MatDialog, MatPaginator, MatSort} from "@angular/material";
import {tap} from "rxjs/operators";
import {PiplineModalWindowComponent} from "./pipline-modal-window/pipline-modal-window.component";
import {DictionaryService} from "../../services/dictionary.service";
import {DictionaryTo} from "../../to/dictionary.to";

@Component({
  selector: 'app-service-log',
  templateUrl: './service-log.component.html',
  styleUrls: ['./service-log.component.sass']
})
export class ServiceLogComponent implements OnInit, AfterViewInit {

  dataSource: PipelineDataSource;
  count: number;
  dataTypes: DictionaryTo[];
  groupTypes: DictionaryTo[];

  displayedColumns = ['id', 'name', 'groupCode', 'host', 'port', 'path', 'inDataTypeId', 'outDataTypeId', 'delete'];
  @ViewChild(MatSort) sorter: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private pipelineService: PiplineservServiceService,
              private dialog: MatDialog,
              private dictionaryService: DictionaryService) {
  }

  ngOnInit() {
    this.dictionaryService.getDataTypes().subscribe(types => {
      this.dataTypes = types;
    });
    this.dictionaryService.getServiceGroupTypes().subscribe(types => {
      this.groupTypes = types;
    });
    this.dataSource = new PipelineDataSource();
    this.updateData();
    this.dataSource.count.subscribe(count => {
      if (count) {
        this.count = count;
      }
    });
  }

  ngAfterViewInit() {
    this.sorter.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sorter.sortChange, this.paginator.page).pipe(tap(() =>
      this.dataSource.update(this.sorter.active, this.sorter.direction, this.paginator.pageIndex, this.paginator.pageSize)
    )).subscribe();
  }

  updateData() {
    this.pipelineService.getAllServices().subscribe((response) => {
      this.dataSource.setData(response, this.sorter.active, this.sorter.direction, this.paginator.pageIndex, this.paginator.pageSize);
    });
  }

  onRowClicked(row, index) {
  }

  onRowDblClicked(row, index) {
    this.dialog.open(PiplineModalWindowComponent, {
      height: '500px', width: '800px',
      data: row
    });
  }

  addService() {
    let dialogRef = this.dialog.open(PiplineModalWindowComponent, {
      height: '500px', width: '800px',
      data: new PipelineServiceTo()
    });
    dialogRef.componentInstance.refreshEvent.subscribe((value) => {
      this.updateData();
    });
  }

  delete(service) {
    this.pipelineService.delete(service.id).subscribe(resp => {
      this.updateData()
    });
  }

  private findItemInGroupDictById(typeCode) {
    let item = this.groupTypes.find((item) => {
      return item.code === typeCode;
    });
    return item ? item.name : null;
  }

  private findItemInDictById(typeCode) {
    let item = this.dataTypes.find((item) => {
      return item.code === typeCode;
    });
    return item ? item.name : null;
  }
}

export class PipelineDataSource extends DataSource<PipelineServiceTo> {
  private dataSubject = new BehaviorSubject<PipelineServiceTo[]>([]);
  public count: BehaviorSubject<number> = new BehaviorSubject(null);
  private data: PipelineServiceTo[];

  constructor() {
    super();
  }

  connect(collectionViewer: CollectionViewer): Observable<PipelineServiceTo[]> {
    return this.dataSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.dataSubject.complete();
  }

  setData(evaluationTasks: PipelineServiceTo[], sortColumnID: string,
          sortDirection: string, pageIndex: number = 0, pageSize: number = 10) {
    this.data = evaluationTasks;
    this.count.next(evaluationTasks.length);
    this.update(sortColumnID, sortDirection, pageIndex, pageSize);
  }

  putElement(record) {
    this.data.push(record);
    this.dataSubject.next(this.data);
  }

  update(sortColumnID: string, sortDirection: string, pageIndex: number = 0, pageSize: number = 10) {
    const compareFn = (a, b) => {
      if (a[sortColumnID] < b[sortColumnID]) {
        return sortDirection == 'asc' ? 1 : -1;
      } else if (a[sortColumnID] > b[sortColumnID]) {
        return sortDirection == 'asc' ? -1 : 1;
      }
      return 0;
    };

    if (sortColumnID) {
      this.data = this.data.sort(compareFn);
    }

    let filtered = this.data.filter((val, index) => (index >= pageIndex * pageSize && index < (pageIndex + 1) * pageSize));
    this.dataSubject.next(filtered);
  }

}
