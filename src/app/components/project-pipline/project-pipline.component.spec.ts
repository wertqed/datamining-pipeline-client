import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectPiplineComponent } from './project-pipline.component';

describe('ProjectPiplineComponent', () => {
  let component: ProjectPiplineComponent;
  let fixture: ComponentFixture<ProjectPiplineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectPiplineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectPiplineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
