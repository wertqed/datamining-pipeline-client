import {Component, OnInit} from '@angular/core';
import {CdkDragDrop, moveItemInArray, copyArrayItem, transferArrayItem} from "@angular/cdk/drag-drop";
import {PiplineservServiceService} from "../../services/piplineserv-service.service";
import {ProjectService} from "../../services/project-service.service";
import {ActivatedRoute} from "@angular/router";
import {ProjectPipelineTo} from "../../to/project-pipeline.to";
import {PipelineStepTo} from "../../to/pipeline-step.to";
import {PipelineServiceTo} from "../../to/pipeline-service.to";
import {ProjectModalWindowComponent} from "../project-modal-window/project-modal-window.component";
import {MatDialog} from "@angular/material";
import {PipelineResultWindowComponent} from "../project-pipeline-graph/pipeline-result-window/pipeline-result-window.component";

@Component({
  selector: 'app-project-pipline',
  templateUrl: './project-pipline.component.html',
  styleUrls: ['./project-pipline.component.sass']
})
export class ProjectPiplineComponent implements OnInit {

  private services = [];

  private pipline = [];

  private projectId;

  constructor(private pipelineService: PiplineservServiceService,
              private projectService: ProjectService,
              private route: ActivatedRoute,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.pipelineService.getAllServices().subscribe((response) => {
      this.services = response;
    });
    this.route.paramMap.subscribe(paramMap => {
      if (paramMap.get('id')) {
        this.projectId = paramMap.get('id');
        this.projectService.getPipeline(paramMap.get('id')).subscribe((response: ProjectPipelineTo) => {
          this.pipline = response.pipelineSteps;
        });
      }
    });
  }

  savePipeline() {
    let proj = new ProjectPipelineTo();
    proj.projectId = this.projectId;
    this.pipline.forEach((val, index) => {
      val.pipeOrd = index;
    });
    proj.pipelineSteps = this.pipline;
    this.projectService.savePipeline(proj).subscribe();
  }

  deleteStep(item) {
    this.pipline = this.pipline.filter((pipe) => {
      return pipe != item;
    });
  }

  startPipeline(item) {
    this.projectService.startPipeline(this.projectId, item.pipeOrd).subscribe(response => {
      let dialogRef = this.dialog.open(PipelineResultWindowComponent, {
        height: '500px', width: '500px',
        data: response.output
      });
    });
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      // @ts-ignore
      let current = event.previousContainer.data[event.previousIndex] as PipelineServiceTo;
      let step = new PipelineStepTo();
      step.name = current.name;
      step.serviceId = current.id;
      this.pipline.push(step);
      // current = step;
      // copyArrayItem(event.previousContainer.data,
      //   event.container.data,
      //   event.previousIndex,
      //   event.currentIndex);
      //TODO потом разобраться с проверками
      // let before = event.currentIndex === 0 ? null : event.container.data[event.currentIndex];
      // let current = event.previousContainer.data[event.previousIndex];
      // let after = event.currentIndex === event.container.data.length ? null : event.container.data[event.currentIndex + 1];
      // if (!before || (before && before["outDataTypeId"] == current["inDataTypeId"])
      //   && !after || (after && after["inDataTypeId"] == current["outDataTypeId"])) {
      //
      // }
    }
  }
}
