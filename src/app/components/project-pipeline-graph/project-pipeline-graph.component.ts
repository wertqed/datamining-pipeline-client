import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ProjectPipelineTo} from "../../to/project-pipeline.to";
import {PiplineservServiceService} from "../../services/piplineserv-service.service";
import {ProjectService} from "../../services/project-service.service";
import {ActivatedRoute} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import * as shape from 'd3-shape';
import {colorSets} from './color-sets';
import {NgxGraphModule} from '@swimlane/ngx-graph';
import {PipelineStepTo} from "../../to/pipeline-step.to";
import {el} from "@angular/platform-browser/testing/src/browser_util";
import {Subject} from "rxjs";
import {PipelineServiceTo} from "../../to/pipeline-service.to";
import {PiplineModalWindowComponent} from "../service-log/pipline-modal-window/pipline-modal-window.component";
import {PipeManageWindowComponent} from "./pipe-manage/pipe-manage-window.component";
import {PipelineLinkTo} from "../../to/pipeline-link.to";
import {UuidUtils} from "../../utils/uuid-utils";
import {DictionaryTo} from "../../to/dictionary.to";
import {DictionaryService} from "../../services/dictionary.service";

@Component({
  selector: 'app-project-pipeline-graph',
  templateUrl: './project-pipeline-graph.component.html',
  styleUrls: ['./project-pipeline-graph.component.sass']
})
export class ProjectPipelineGraphComponent implements OnInit {

  hierarchicalGraph = {nodes: [], links: []};

  view: any[];
  width = 1000;
  height = 600;
  autoZoom = true;
  panOnZoom = true;
  enableZoom = true;
  autoCenter = true;

  update$: Subject<boolean> = new Subject();
  center$: Subject<boolean> = new Subject();
  zoomToFit$: Subject<boolean> = new Subject();

  // options
  showLegend = false;
  orientation = 'LR'; // LR, RL, TB, BT

  // line interpolation
  curveType = 'Linear';
  curve = shape.curveBundle.beta(1);

  colorSchemes: any;
  colorScheme: any;
  schemeType = 'ordinal';
  selectedColorScheme: string;
  private groupService: DictionaryTo[] = [];
  private services: PipelineServiceTo[] = [];
  private pipelineSteps: PipelineStepTo[] = [];
  private pipelineLinks: PipelineLinkTo[] = [];
  private nodes = [];
  private links = [];
  private projectId;
  private selectedService: PipelineServiceTo;
  private input: PipelineStepTo;

  private selectedGroup: DictionaryTo;
  private selectedData: any;
  private selectedStepInput: PipelineStepTo;

  constructor(private pipelineService: PiplineservServiceService,
              private projectService: ProjectService,
              private dictionaryService: DictionaryService,
              private route: ActivatedRoute,
              private dialog: MatDialog) {
    Object.assign(this, {
      colorSchemes: colorSets,
    });

    this.setColorScheme('picnic');
    this.setInterpolationType('Bundle');
  }

  ngOnInit() {
    this.pipelineService.getAllServices().subscribe((response) => {
      this.services = response;
    });
    this.dictionaryService.getServiceGroupTypes().subscribe(types => {
      this.groupService = types;
    });
    this.route.paramMap.subscribe(paramMap => {
      if (paramMap.get('id')) {
        this.projectId = paramMap.get('id');
        this.projectService.getPipeline(paramMap.get('id')).subscribe((response: ProjectPipelineTo) => {
          this.pipelineSteps = response.pipelineSteps;
          this.pipelineLinks = response.pipelineLinks;
          this.updateChart();
        });
      }
    });
  }

  select(node) {
    let currentStep = this.pipelineSteps.find(elem => {
      return elem.uuid == node.id;
    });
    this.selectedData = {
      node: node,
      currentStep: currentStep,
      service: this.services.find(s => s.id === currentStep.serviceId),
      services: this.services,
      pipelineSteps: this.pipelineSteps,
      pipelineLinks: this.pipelineLinks,
      hierarchicalGraph: this.hierarchicalGraph,
      update: this.update$,
      links: this.links,
      projectId: this.projectId
    };
    let link = this.pipelineLinks.find(pl => {
      return pl.targetNodeUuid == currentStep.uuid;
    });
    if (link) {
      this.selectedStepInput = this.pipelineSteps.find(step => {
        return step.uuid == link.sourceNodeUuid;
      });
    }
  }

  setColorScheme(name) {
    this.selectedColorScheme = name;
    this.colorScheme = this.colorSchemes.find(s => s.name === name);
  }

  setInterpolationType(curveType) {
    this.curveType = curveType;
    if (curveType === 'Bundle') {
      this.curve = shape.curveBundle.beta(1);
    }
    if (curveType === 'Cardinal') {
      this.curve = shape.curveCardinal;
    }
    if (curveType === 'Catmull Rom') {
      this.curve = shape.curveCatmullRom;
    }
    if (curveType === 'Linear') {
      this.curve = shape.curveLinear;
    }
    if (curveType === 'Monotone X') {
      this.curve = shape.curveMonotoneX;
    }
    if (curveType === 'Monotone Y') {
      this.curve = shape.curveMonotoneY;
    }
    if (curveType === 'Natural') {
      this.curve = shape.curveNatural;
    }
    if (curveType === 'Step') {
      this.curve = shape.curveStep;
    }
    if (curveType === 'Step After') {
      this.curve = shape.curveStepAfter;
    }
    if (curveType === 'Step Before') {
      this.curve = shape.curveStepBefore;
    }
  }

  onLegendLabelClick(entry) {
    console.log('Legend clicked', entry);
  }

  updateChart() {
    this.nodes = [];
    this.links = [];
    this.pipelineSteps.forEach(elem => {
      this.nodes.push({
        id: elem.uuid,
        label: elem.name,
      })
    });
    this.pipelineLinks.forEach(elem => {
      this.links.push({
        source: elem.sourceNodeUuid,
        target: elem.targetNodeUuid,
      })
    });
    this.hierarchicalGraph.nodes = this.nodes;
    this.hierarchicalGraph.links = this.links;
    this.update$.next(true);
  }

  savePipeline() {
    let proj = new ProjectPipelineTo();
    proj.projectId = this.projectId;
    proj.pipelineSteps = this.pipelineSteps;
    proj.pipelineLinks = this.pipelineLinks;
    this.projectService.savePipeline(proj).subscribe();
  }

  addNode() {
    let pipe = new PipelineStepTo();
    pipe.name = this.selectedService.name;
    pipe.serviceId = this.selectedService.id;
    pipe.uuid = UuidUtils.genUuid();
    pipe.metaInfo = this.selectedService.metaInfo;

    this.pipelineSteps.push(pipe);
    this.nodes.push({
      id: pipe.uuid,
      label: this.selectedService.name
    });
    if (this.input != null) {
      this.links.push({
        source: this.input.uuid,
        target: pipe.uuid,
      });
      let link = new PipelineLinkTo();
      link.sourceNodeUuid = this.input.uuid;
      link.targetNodeUuid = pipe.uuid;
      this.pipelineLinks.push(link);
    }
    this.updateChart();
  }

  deleteCurrentStep() {
    let linkIndex;
    let link = this.pipelineLinks.find((pl, index) => {
      linkIndex = index;
      return pl.targetNodeUuid == this.selectedData.currentStep.uuid;
    });
    if (link) {
      this.pipelineLinks.splice(linkIndex, 1);
    }
    let stepIndex = this.pipelineSteps.findIndex((step, index) => {
      return step.uuid == this.selectedData.currentStep.uuid;
    });

    let linksList = this.pipelineLinks.filter(pl => {
      return pl.sourceNodeUuid == this.selectedData.currentStep.uuid;
    });
    if (linksList.length > 0) {
      linksList.forEach(elem => {
        this.pipelineLinks.splice(this.pipelineLinks.indexOf(elem), 1);
      });
    }
    this.pipelineSteps.splice(stepIndex, 1);
    this.updateChart();
  }

  filterServices(){
    if (this.selectedGroup) {
      return this.services.filter(s => {
        return s.groupCode == this.selectedGroup.code;
      });
    } else {
      return this.services;
    }
  }

  filterPipeSteps() {
    if (this.selectedService) {
      return this.pipelineSteps.filter(s => {
        let service = this.services.find(service => {
          return service.id == s.serviceId;
        });
        return service.outDataTypeCode == this.selectedService.inDataTypeCode;
      });
    } else {
      return [];
    }
  }
}
