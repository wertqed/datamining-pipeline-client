import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {PipelineServiceTo} from "../../../to/pipeline-service.to";
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-pipeline-result-window',
  templateUrl: './pipeline-result-window.component.html',
  styleUrls: ['./pipeline-result-window.component.sass']
})
export class PipelineResultWindowComponent implements OnInit {

  result: any;
  type: string;
  imagePath: any;

  respCnt: number;

  constructor(public dialogRef: MatDialogRef<PipelineResultWindowComponent>,
              @Inject(MAT_DIALOG_DATA) public res: any,
              private _sanitizer: DomSanitizer) {
    this.type = res.type;
    if (this.type == 'IMAGE') {
      this.result = res.response;
      this.imagePath = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,'
        + this.result);
    } else if (this.type == 'TWO_DIM_ARRAY') {
      this.respCnt = res.response.length;
      this.result = res.response.length <= 200 ? res.response : res.response.slice(1, 100);
    } else {
      this.result = res.response;
    }
  }

  ngOnInit() {
  }

}
