import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PipelineResultWindowComponent } from './pipeline-result-window.component';

describe('PipelineResultWindowComponent', () => {
  let component: PipelineResultWindowComponent;
  let fixture: ComponentFixture<PipelineResultWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PipelineResultWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PipelineResultWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
