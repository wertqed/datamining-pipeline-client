import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MetaInfoInputComponent } from './meta-info-input.component';

describe('MetaInfoInputComponent', () => {
  let component: MetaInfoInputComponent;
  let fixture: ComponentFixture<MetaInfoInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MetaInfoInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetaInfoInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
