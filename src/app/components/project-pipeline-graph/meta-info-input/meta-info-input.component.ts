import {Component, Input, OnInit} from '@angular/core';
import {PipelineMetainfoTo} from "../../../to/pipeline-metainfo.to";

@Component({
  selector: 'app-meta-info-input',
  templateUrl: './meta-info-input.component.html',
  styleUrls: ['./meta-info-input.component.sass']
})
export class MetaInfoInputComponent implements OnInit {

  @Input()
  record: PipelineMetainfoTo;

  fileList: FileList;

  constructor() {
  }

  ngOnInit() {
  }

  handleFileInput(files: FileList) {
    let file = files.item(0);
    this.fileList = files;
    this.record.fileName = file.name;
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      if (typeof reader.result === "string") {
        this.record.value = reader.result.replace(/^data:.+;base64,/, '');
      };
    };
  }
}
