import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectPipelineGraphComponent } from './project-pipeline-graph.component';

describe('ProjectPipelineGraphComponent', () => {
  let component: ProjectPipelineGraphComponent;
  let fixture: ComponentFixture<ProjectPipelineGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectPipelineGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectPipelineGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
