import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {ProjectPipelineTo} from "../../../to/project-pipeline.to";
import {PipelineResultWindowComponent} from "../pipeline-result-window/pipeline-result-window.component";
import {PipelineServiceTo} from "../../../to/pipeline-service.to";
import {PiplineservServiceService} from "../../../services/piplineserv-service.service";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {ProjectService} from "../../../services/project-service.service";
import {PipelineStepTo} from "../../../to/pipeline-step.to";
import {PipelineLinkTo} from "../../../to/pipeline-link.to";

@Component({
  selector: 'app-pipe-manage-window',
  templateUrl: './pipe-manage-window.component.html',
  styleUrls: ['./pipe-manage-window.component.sass']
})
export class PipeManageWindowComponent {
  @Input("data")
  public data: any;
  @Input("input")
  public input: PipelineStepTo;
  @Output()
  public delete = new EventEmitter();

  constructor(private projectService: ProjectService,
              private dialog: MatDialog) {
  }

  inputChange(stepTo: PipelineStepTo) {
    let link = this.data.pipelineLinks.find(pl => {
      return pl.targetNodeUuid == this.data.currentStep.uuid;
    });
    if (link) {
      link.sourceNodeUuid = stepTo.uuid;
      this.data.links.find(lin => {
        return lin.target == this.data.currentStep.uuid;
      }).source = stepTo.uuid;
    } else {
      this.data.pipelineLinks.push({
        sourceNodeUuid: stepTo.uuid,
        targetNodeUuid: this.data.currentStep.uuid
      });
      this.data.links.push({
        source: stepTo.uuid,
        target: this.data.currentStep.uuid
      });
    }
    this.data.hierarchicalGraph.links = this.data.links;
    this.data.update.next(true);
  }

  startPipeline() {
    this.projectService.startPipeline(this.data.projectId, this.data.currentStep.uuid).subscribe(response => {
      let dialogRef = this.dialog.open(PipelineResultWindowComponent, {
        height: '700px', width: '800px',
        data: {
          type: this.data.service.outDataTypeCode,
          response: response.output
        },
      });
    });
  }

  getPipelineResult() {
    this.projectService.getResult(this.data.currentStep.uuid).subscribe(response => {
      let dialogRef = this.dialog.open(PipelineResultWindowComponent, {
        height: '700px', width: '800px',
        data: {
          type: this.data.service.outDataTypeCode,
          response: response.output
        },
      });
    });
  }

  deleteStep() {
    this.data = null;
    this.input = null;
    this.delete.emit();
  }

  filterPipeSteps() {
    if (this.data.service) {
      return this.data.pipelineSteps.filter(s => {
        let service = this.data.services.find(service => {
          return service.id == s.serviceId;
        });
        return service.outDataTypeCode == this.data.service.inDataTypeCode;
      });
    } else {
      return [];
    }
  }
}
