import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PipeManageWindowComponent } from './pipe-manage-window.component';

describe('PipeManageWindowComponent', () => {
  let component: PipeManageWindowComponent;
  let fixture: ComponentFixture<PipeManageWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PipeManageWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PipeManageWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
