import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {PipelineServiceTo} from "../../to/pipeline-service.to";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {PiplineservServiceService} from "../../services/piplineserv-service.service";
import {ProjectTo} from "../../to/project.to";
import {ProjectService} from "../../services/project-service.service";

@Component({
  selector: 'app-project-modal-window',
  templateUrl: './project-modal-window.component.html',
  styleUrls: ['./project-modal-window.component.sass']
})
export class ProjectModalWindowComponent implements OnInit {

  record: ProjectTo;
  @Output()
  public refreshEvent: EventEmitter<null> = new EventEmitter();

  constructor(public dialogRef: MatDialogRef<ProjectModalWindowComponent>,
              @Inject(MAT_DIALOG_DATA) public inRecord: PipelineServiceTo,
              private projectService: ProjectService) {
    this.record = inRecord;
  }

  ngOnInit() {
  }

  save(){
    this.projectService.save(this.record).subscribe(resp=>{
      this.refreshEvent.emit();
    });
  }
}
