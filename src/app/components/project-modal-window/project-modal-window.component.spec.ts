import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectModalWindowComponent } from './project-modal-window.component';

describe('ProjectModalWindowComponent', () => {
  let component: ProjectModalWindowComponent;
  let fixture: ComponentFixture<ProjectModalWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectModalWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectModalWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
