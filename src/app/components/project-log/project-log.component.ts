import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort} from "@angular/material";
import {PiplineservServiceService} from "../../services/piplineserv-service.service";
import {BehaviorSubject, merge, Observable} from "rxjs";
import {tap} from "rxjs/operators";
import {PiplineModalWindowComponent} from "../service-log/pipline-modal-window/pipline-modal-window.component";
import {PipelineServiceTo} from "../../to/pipeline-service.to";
import {DataSource} from "@angular/cdk/table";
import {CollectionViewer} from "@angular/cdk/collections";
import {ProjectService} from "../../services/project-service.service";
import {ProjectTo} from "../../to/project.to";
import {ProjectModalWindowComponent} from "../project-modal-window/project-modal-window.component";
import {Router} from "@angular/router";

@Component({
  selector: 'app-project-log',
  templateUrl: './project-log.component.html',
  styleUrls: ['./project-log.component.sass']
})
export class ProjectLogComponent implements OnInit {

  dataSource: ProjectDataSource;
  count: number;

  displayedColumns = ['id', 'name', 'edit', 'delete'];
  @ViewChild(MatSort) sorter: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private projectService: ProjectService,
              private dialog: MatDialog,
              private router: Router) {
  }

  ngOnInit() {
    this.dataSource = new ProjectDataSource();
    this.updateData();
    this.dataSource.count.subscribe(count => {
      if (count) {
        this.count = count;
      }
    });
  }

  ngAfterViewInit() {
    this.sorter.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sorter.sortChange, this.paginator.page).pipe(tap(() =>
      this.dataSource.update(this.sorter.active, this.sorter.direction, this.paginator.pageIndex, this.paginator.pageSize)
    )).subscribe();
  }

  updateData() {
    this.projectService.getAllProjects().subscribe((response) => {
      this.dataSource.setData(response, this.sorter.active, this.sorter.direction, this.paginator.pageIndex, this.paginator.pageSize);
    });
  }

  onRowClicked(row, index) {
  }

  onRowDblClicked(row, index) {
    this.dialog.open(ProjectModalWindowComponent, {
      height: '500px', width: '800px',
      data: row
    });
  }

  addProject() {
    let dialogRef = this.dialog.open(ProjectModalWindowComponent, {
      height: '500px', width: '800px',
      data: new PipelineServiceTo()
    });
    dialogRef.componentInstance.refreshEvent.subscribe((value) => {
      this.updateData();
    });
  }

  edit(project) {
    // this.router.navigateByUrl('project-pipeline/' + project.id);
    this.router.navigateByUrl('project-pipeline-network/' + project.id);
  }

  delete(project) {
    this.projectService.delete(project.id).subscribe(resp => {
      this.updateData()
    });
  }
}

export class ProjectDataSource extends DataSource<ProjectTo> {
  private dataSubject = new BehaviorSubject<ProjectTo[]>([]);
  public count: BehaviorSubject<number> = new BehaviorSubject(null);
  private data: ProjectTo[];

  constructor() {
    super();
  }

  connect(collectionViewer: CollectionViewer): Observable<ProjectTo[]> {
    return this.dataSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.dataSubject.complete();
  }

  setData(evaluationTasks: PipelineServiceTo[], sortColumnID: string,
          sortDirection: string, pageIndex: number = 0, pageSize: number = 10) {
    this.data = evaluationTasks;
    this.count.next(evaluationTasks.length);
    this.update(sortColumnID, sortDirection, pageIndex, pageSize);
  }

  putElement(record) {
    this.data.push(record);
    this.dataSubject.next(this.data);
  }

  update(sortColumnID: string, sortDirection: string, pageIndex: number = 0, pageSize: number = 10) {
    const compareFn = (a, b) => {
      if (a[sortColumnID] < b[sortColumnID]) {
        return sortDirection == 'asc' ? 1 : -1;
      } else if (a[sortColumnID] > b[sortColumnID]) {
        return sortDirection == 'asc' ? -1 : 1;
      }
      return 0;
    };

    if (sortColumnID) {
      this.data = this.data.sort(compareFn);
    }

    let filtered = this.data.filter((val, index) => (index >= pageIndex * pageSize && index < (pageIndex + 1) * pageSize));
    this.dataSubject.next(filtered);
  }
}
